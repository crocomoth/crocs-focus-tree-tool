﻿namespace CrocFocusTreeTool
{
    internal static class ConsoleWriter
    {
        private static ConsoleColor consoleColor = Console.ForegroundColor;

        public static void WriteLine(string message, ConsoleColor color = ConsoleColor.Green)
        {
            if (color != consoleColor)
            {
                Console.ForegroundColor = color;
                consoleColor = color;
            }

            Console.WriteLine(message);
        }

        public static void WriteLineOnceWithColor(string message, ConsoleColor color)
        {
            Console.ForegroundColor = color;
            Console.WriteLine(message);
            Console.ForegroundColor = consoleColor;
        }
    }
}
