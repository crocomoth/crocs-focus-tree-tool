﻿using CrocFocusTreeTool.Models;
using CrocFocusTreeTool.Services;

namespace CrocFocusTreeTool
{
    public class Program
    {
        public static void Main(string[] args)
        {
            ConsoleWriter.WriteLine("Welcome to Croc's Focus Tree Tool! Type 'help' for more detailed info");

            var service = new MainService();

            do
            {
                try
                {
                    ConsoleWriter.WriteLine("Input path to the xml file");
                    var input = GetInput();

                    // for now
                    //ConsoleWriter.WriteLine("Input path to focus tree rewards file");
                    //var reward = GetInput();

                    ConsoleWriter.WriteLine("Input path to folder where result will be stored");
                    var output = GetInput();

                    var options = ProcessTagString();

                    service.Process(input, string.Empty, output, options);
                    ConsoleWriter.WriteLine("Finished processing", ConsoleColor.Magenta);
                }
                catch (Exception e)
                {
                    ConsoleWriter.WriteLineOnceWithColor(e.Message, ConsoleColor.Red);
                }
            } while (true);
        }

        private static string GetInput()
        {
            do
            {
                var input = Console.ReadLine();
                if (string.IsNullOrWhiteSpace(input))
                {
                    ConsoleWriter.WriteLineOnceWithColor("Data entered incorrectly", ConsoleColor.Red);
                    continue;
                }

                if (input.Equals(Constants.Commands.Help))
                {
                    WriteHelp();
                    continue;
                }

                return input;
            } while (true);
        }

        private static ProcessingOptions ProcessTagString()
        {
            ConsoleWriter.WriteLine("Input tag for a country and options if needed");
            var tagString = GetInput();

            var parts = tagString.Split(Constants.Commands.CommandsDelimeter, StringSplitOptions.RemoveEmptyEntries).ToList();

            parts = parts.Select(x => x.Trim()).ToList();

            var tag = parts[0].ToUpper();

            var customName = GetValueFromString(parts, Constants.Commands.Name) ?? $"{tag.ToLower()}_focus";

            var coordinatesDelimeterX = GetValueFromString(parts, Constants.Commands.XDel);
            int delX = GetIntValueOrDefault(coordinatesDelimeterX, 40);

            var coordinatesDelimeterY = GetValueFromString(parts, Constants.Commands.YDel);
            int delY = GetIntValueOrDefault(coordinatesDelimeterY, 40);

            var continiousPosX = GetValueFromString(parts, Constants.Commands.XCont);
            int posX = GetIntValueOrDefault(continiousPosX, 1000);

            var continiousPosY = GetValueFromString(parts, Constants.Commands.YCont);
            int posY = GetIntValueOrDefault(continiousPosY, 100);

            var customIcon = GetValueFromString(parts, Constants.Commands.Icon) ?? "null";

            var defaultAiWeight = GetValueFromString(parts, Constants.Commands.Ai);
            int aiWeight = GetIntValueOrDefault(defaultAiWeight, 1);

            var costString = GetValueFromString(parts, Constants.Commands.Cost);
            int cost = GetIntValueOrDefault(costString, 10);

            var defaultFocusFilters = GetValueFromString(parts, Constants.Commands.Filter);
            if (string.IsNullOrEmpty(defaultFocusFilters))
            {
                defaultFocusFilters = "FOCUS_FILTER_POLITICAL";
            }

            var options = new ProcessingOptions(tag, customName, posX, posY, customIcon, aiWeight, cost, delX, delY)
            {
                DefaultFocusFilters = defaultFocusFilters
            };

            return options;
        }

        private static string? GetValueFromString(List<string> source, string part)
        {
            var target = source.FirstOrDefault(x => x.StartsWith(part));
            if (target is null)
            {
                return null;
            }

            var parts = target.Split(Constants.Commands.ValueDelimeter);
            if (parts.Length != 2)
            {
                throw new Exception("string has incorrect format");
            }

            var value = parts[1].Trim();
            return value;
        }

        private static int GetIntValueOrDefault(string? source, int defaultValue)
        {
            if (source is not null)
            {
                if (int.TryParse(source, out int val))
                {
                    return val;
                }
            }

            return defaultValue;
        }

        private static void WriteHelp()
        {
            ConsoleWriter.WriteLine("After entering source and target file you can specify additional values", ConsoleColor.Blue);
            ConsoleWriter.WriteLine("First value should be tag, without any edits", ConsoleColor.Blue);
            ConsoleWriter.WriteLine("They should be split using ';' char and be in format 'key=value', they are trimmed when processed", ConsoleColor.Blue);
            ConsoleWriter.WriteLine("name - custom name for focus tree", ConsoleColor.Blue);
            ConsoleWriter.WriteLine("x-del - how much X coordinate from xml should be decreased in output", ConsoleColor.Blue);
            ConsoleWriter.WriteLine("y-del - how much Y coordinate from xml should be decreased in output", ConsoleColor.Blue);
            ConsoleWriter.WriteLine("x-cont - X coordinate of continious focus", ConsoleColor.Blue);
            ConsoleWriter.WriteLine("y-cont - Y coordinate of continious focus", ConsoleColor.Blue);
            ConsoleWriter.WriteLine("icon - name of default icon for focus", ConsoleColor.Blue);
            ConsoleWriter.WriteLine("ai -  default ai weight for focuses", ConsoleColor.Blue);
            ConsoleWriter.WriteLine("filter - default focus tree filters", ConsoleColor.Blue);
            ConsoleWriter.WriteLine("cost - default cost for focuses", ConsoleColor.Blue);
        }
    }
}
