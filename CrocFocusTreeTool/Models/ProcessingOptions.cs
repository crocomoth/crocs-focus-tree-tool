﻿namespace CrocFocusTreeTool.Models
{
    public class ProcessingOptions
    {
        public ProcessingOptions(string tag, string focusTreeName, int continuousFocusX, int continuousFocusY, string icon, int defaultAiWeight, int cost, float coordinatesDelimeterX, float coordinatesDelimeterY)
        {
            Tag = tag;
            FocusTreeName = focusTreeName;
            ContinuousFocusX = continuousFocusX;
            ContinuousFocusY = continuousFocusY;
            Icon = icon;
            DefaultAiWeight = defaultAiWeight;
            Cost = cost;
            CoordinatesDelimeterX = coordinatesDelimeterX;
            CoordinatesDelimeterY = coordinatesDelimeterY;
        }

        public string Tag { get; set; }
        public string FocusTreeName { get; set; }
        public float CoordinatesDelimeterX { get; set; }
        public float CoordinatesDelimeterY { get; set; }
        public int ContinuousFocusX { get; set; }
        public int ContinuousFocusY { get; set; }
        public string Icon { get; set; }
        public int DefaultAiWeight { get; set; }
        public string? DefaultFocusFilters { get; set; }
        public int Cost { get; set; }
    }
}
