﻿namespace CrocFocusTreeTool.Models
{
    public class Line
    {
        public Line(string? source, string? target, bool isDotted = false, bool isLink = false)
        {
            if (string.IsNullOrWhiteSpace(source))
            {
                throw new ArgumentException("Argument is empty", nameof(source));
            }

            if (string.IsNullOrWhiteSpace(target))
            {
                throw new ArgumentException("Argument is empty", nameof(target));
            }

            Source = source;
            Target = target;
            Dotted = isDotted;
            IsLink = isLink;
        }

        public string Source { get; set; }
        public string Target { get; set; }
        public bool Dotted { get; set; }
        public bool IsLink { get; set; }
    }
}
