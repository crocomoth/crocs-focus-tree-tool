﻿namespace CrocFocusTreeTool.Models
{
    public class Focus
    {
        public Focus(string? name, string? id, int? x, int? y)
        {
            if (string.IsNullOrWhiteSpace(name))
            {
                throw new ArgumentException("Argument was empty:", nameof(name));
            }

            if (string.IsNullOrWhiteSpace(id))
            {
                throw new ArgumentException("Argument was empty:", nameof(id));
            }

            if (x is null)
            {
                throw new ArgumentException("Argument was null:", nameof(id));
            }

            if (y is null)
            {
                throw new ArgumentException("Argument was null:", nameof(id));
            }

            Name = name;
            MutuallyExcusiveWith = new List<string>();
            Prerequisites = new List<string>();
            NonDisablingPrerequisites = new List<string>();
            Id = id;
            X = x.Value;
            Y = y.Value;
        }

        public string Id { get; set; }
        public int X { get; set; }
        public int Y { get; set; }
        public int RelativeX { get; set; }
        public int RelativeY { get; set; }
        public string Name { get; set; }
        public string? ParentName { get; set; }
        public List<string> MutuallyExcusiveWith { get; set; }
        public List<string> Prerequisites { get; set; }
        public List<string> NonDisablingPrerequisites { get; set; }
    }
}
