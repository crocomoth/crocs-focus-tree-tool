﻿using CrocFocusTreeTool.Models;
using System.Text;

namespace CrocFocusTreeTool.Services
{
    public class LocalisationService
    {
        public string CreateLocalisation(List<Focus> focuses)
        {
            var builder = new StringBuilder();
            foreach (var focus in focuses)
            {
                var content = focus.Name.Replace('_', ' ');
                builder.AppendLine($"{focus.Name}:\"{content}\"");
                builder.AppendLine($"{focus.Name}_desc:\"\"");
            }

            return builder.ToString();
        }
    }
}
