﻿using CrocFocusTreeTool.Models;
using System.Text;

namespace CrocFocusTreeTool.Services
{
    public class DocumentProcessor
    {
        private readonly FocusTreeRewardService _rewardService;

        public DocumentProcessor(FocusTreeRewardService rewardService)
        {
            _rewardService = rewardService;
        }

        public string CreateDocument(List<Focus> focuses, ProcessingOptions options)
        {
            StringBuilder builder = new();

            builder.AppendLine("focus_tree = {");

            SetUpTop(options, builder);
            SetUpContiniousFocus(options, builder);

            builder.AppendLine();

            foreach (var focus in focuses)
            {
                builder.AppendLine("focus = {".Tab(1));
                builder.AppendLine($"id = {focus.Name}".Tab(2));

                SetIcon(options, builder);
                SetUpCoordinates(builder, focus);
                SetUpPrerequisites(builder, focus);
                SetUpMutuals(builder, focus);
                SetUpCost(options, builder);
                SetUpFilters(options, builder);
                SetUpCompletionReward(builder, focus);
                SetUpAiWillDo(options, builder);

                builder.AppendLine("}".Tab(1));
                builder.AppendLine();
            }

            builder.AppendLine("}");

            return builder.ToString();
        }

        private static void SetUpFilters(ProcessingOptions options, StringBuilder builder)
        {
            if (options.DefaultFocusFilters is not null)
            {
                builder.AppendLine($"search_filters = {{ {options.DefaultFocusFilters} }}".Tab(2));
            }
        }

        private static void SetUpCost(ProcessingOptions options, StringBuilder builder)
        {
            builder.AppendLine($"cost = {options.Cost}".Tab(2));
            builder.AppendLine();
        }

        private static void SetUpAiWillDo(ProcessingOptions options, StringBuilder builder)
        {
            builder.AppendLine($"ai_will_do = {{ factor = {options.DefaultAiWeight} }}".Tab(2));
        }

        private void SetUpCompletionReward(StringBuilder builder, Focus focus)
        {
            builder.AppendLine("completion_reward = {".Tab(2));
            builder.AppendLine($"log = \"[GetDateText]: [Root.GetName]: Focus {focus.Name}\"".Tab(3));
            var rewards = _rewardService.GetRewardForFocus(focus);
            foreach (var reward in rewards)
            {
                builder.AppendLine(reward.Tab(3));
            }

            builder.AppendLine("}".Tab(2));

            builder.AppendLine();
        }

        private static void SetUpMutuals(StringBuilder builder, Focus focus)
        {
            if (focus.MutuallyExcusiveWith.Any())
            {
                if (focus.MutuallyExcusiveWith.Count == 1)
                {
                    builder.AppendLine($"mutually_exclusive = {{ focus = {focus.MutuallyExcusiveWith.First()} }}".Tab(2));
                }
                else
                {
                    builder.AppendLine("mutually_exclusive = {".Tab(2));

                    foreach (var mut in focus.MutuallyExcusiveWith)
                    {
                        builder.AppendLine($"focus = {mut}".Tab(3));
                    }

                    builder.AppendLine("}".Tab(2));
                    builder.AppendLine();
                }
            }
        }

        private static void SetUpPrerequisites(StringBuilder builder, Focus focus)
        {
            if (focus.NonDisablingPrerequisites.Any())
            {
                builder.AppendLine("prerequisite = {".Tab(2));
                foreach (var prer in focus.NonDisablingPrerequisites)
                {
                    builder.AppendLine($"focus = {prer}".Tab(3));
                }

                builder.AppendLine("}".Tab(2));
                builder.AppendLine();
            }

            if (focus.Prerequisites.Any())
            {
                foreach (var prer in focus.Prerequisites)
                {
                    builder.AppendLine($"prerequisite = {{ focus = {prer} }}".Tab(2));
                }

                builder.AppendLine();
            }
        }

        private static void SetUpCoordinates(StringBuilder builder, Focus focus)
        {
            builder.AppendLine($"x = {focus.RelativeX}".Tab(2));
            builder.AppendLine($"y = {focus.RelativeY}".Tab(2));
            builder.AppendLine();

            if (focus.ParentName is not null)
            {
                builder.AppendLine($"relative_position_id = {focus.ParentName}".Tab(2));
                builder.AppendLine();
            }
        }

        private static void SetUpContiniousFocus(ProcessingOptions options, StringBuilder builder)
        {
            builder.AppendLine($"continuous_focus_position = {{ x = {options.ContinuousFocusX} y = {options.ContinuousFocusY} }}".Tab(1));
        }

        private static void SetUpTop(ProcessingOptions options, StringBuilder builder)
        {
            builder.AppendLine($"id = {options.FocusTreeName}".Tab(1));
            builder.AppendLine("country = {".Tab(1));
            builder.AppendLine("factor = 0".Tab(2));
            builder.AppendLine("modifier = {".Tab(2));
            builder.AppendLine("add = 20".Tab(3));
            builder.AppendLine($"original_tag = {options.Tag}".Tab(3));
            builder.AppendLine("}".Tab(2));
            builder.AppendLine("}".Tab(1));
            builder.AppendLine();
        }

        private static void SetIcon(ProcessingOptions options, StringBuilder builder)
        {
            builder.AppendLine($"icon = {options.Icon}".Tab(2));
            builder.AppendLine();
        }
    }
}
