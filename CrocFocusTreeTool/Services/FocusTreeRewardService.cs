﻿using CrocFocusTreeTool.Models;
using Newtonsoft.Json;

namespace CrocFocusTreeTool.Services
{
    public class FocusTreeRewardService
    {
        // contains tokens
        private readonly Dictionary<string, string> simpleRewards;
        // contains focuses and tokenized rewards
        private readonly Dictionary<string, string> focusRewards;

        public FocusTreeRewardService()
        {
            simpleRewards = new Dictionary<string, string>();
            focusRewards = new Dictionary<string, string>();
        }

        public void SetUpTokens(IEnumerable<string> lines)
        {
            foreach (var line in lines)
            {
                var parts = line.Split(':', StringSplitOptions.TrimEntries);
                if (parts.Length != 2)
                {
                    ConsoleWriter.WriteLineOnceWithColor($"focus has incorrect format: {line}", ConsoleColor.Yellow);
                    continue;
                }

                focusRewards.Add(parts[0], parts[1]);
            }
        }

        public void SetUpSimpleRewards(string content)
        {
            var rewards = JsonConvert.DeserializeObject<Dictionary<string, string>>(content);

            if (rewards is null)
            {
                ConsoleWriter.WriteLineOnceWithColor($"Could not parse simple rewards", ConsoleColor.Yellow);
                return;
            }

            foreach (var reward in rewards)
            {
                simpleRewards.Add(reward.Key, reward.Value);
            }
        }

        /// <summary>
        /// Maps focus tree rewards to tokens.
        /// </summary>
        /// <param name="focus">Focus to process.</param>
        /// <returns>Completion reward string.</returns>
        public List<string> GetRewardForFocus(Focus focus)
        {
            var result = new List<string>();

            if(!focusRewards.TryGetValue(focus.Name, out var rewardLine))
            {
                ConsoleWriter.WriteLineOnceWithColor($"Completion rewards: focus was not found: {focus.Name}", ConsoleColor.Yellow);
                return new();
            }

            var parts = rewardLine.Split(';', StringSplitOptions.TrimEntries);
            if (parts.Length == 0)
            {
                return new();
            }

            foreach (var part in parts)
            {
                var subparts = part.Split('=', StringSplitOptions.TrimEntries);
                ProcessSubPart(subparts.ToList(), part, result);
            }

            return result;
        }

        public void ProcessSubPart(List<string> subparts, string fullLine, List<string> result)
        {
            if (subparts.Count == 1)
            {
                if (simpleRewards.TryGetValue(subparts[0], out var rewardLine))
                {
                    result.Add(rewardLine);
                    return;
                }

                ConsoleWriter.WriteLineOnceWithColor($"Could bot find simple reward: {subparts[0]}", ConsoleColor.Yellow);
                return;
            }

            switch (subparts[0].ToLower())
            {
                case "political power":
                    result.Add($"add_political_power = {subparts[1]}");
                    break;
                default:
                    ConsoleWriter.WriteLineOnceWithColor($"Could not parse complex reward: {subparts[0]}", ConsoleColor.Yellow);
                    // for unknown scripted effects
                    result.Add(fullLine);
                    break;
            }
        }
    }
}
