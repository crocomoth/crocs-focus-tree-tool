﻿using CrocFocusTreeTool.Models;
using System.Text.RegularExpressions;

namespace CrocFocusTreeTool.Services
{
    public class TreeProcessor
    {
        public List<Focus> Process(Mxfile mxfile, ProcessingOptions options)
        {
            var focuses = GetFocuses(mxfile, options);
            var lines = GetLines(mxfile);

            PopulateRelations(focuses, lines);
            SetUpRelationalCoordinates(focuses, options);

            return focuses;
        }

        private List<Focus> GetFocuses(Mxfile mxfile, ProcessingOptions options)
        {
            var items = GetItems(mxfile);

            // lines don't have vertex
            var focuses = items.Where(x => x.Vertex != 0 && !string.IsNullOrWhiteSpace(x.Value));

            var result = focuses.Select(x => new Focus(ProcessFocusName(x.Value, options), x.Id, (int?)x.MxGeometry?.X, (int?)x.MxGeometry?.Y)).ToList();
            return result;
        }

        private List<Line> GetLines(Mxfile mxfile)
        {
            var items = GetItems(mxfile);

            var lines = items.Where(x => x.Source is not null && x.Target is not null);

            var result = lines.Select(x => new Line(x.Source, x.Target, ProcessDash(x.Style), ProcessLink(x.Style))).ToList();

            return result;
        }

        private void PopulateRelations(List<Focus> focuses, List<Line> lines)
        {
            foreach (var line in lines)
            {
                var source = focuses.FirstOrDefault(x => x.Id.Equals(line.Source));
                var target = focuses.FirstOrDefault(x => x.Id.Equals(line.Target));
                if (source is null || target is null)
                {
                    throw new Exception("One of lines is not set up correctly");
                }

                if (line.IsLink)
                {
                    target.MutuallyExcusiveWith.Add(source.Name);
                    source.MutuallyExcusiveWith.Add(target.Name);
                }
                else
                {
                    if (line.Dotted)
                    {
                        target.NonDisablingPrerequisites.Add(source.Name);
                    }
                    else
                    {
                        target.Prerequisites.Add(source.Name);
                    }
                }
            }
        }

        private void SetUpRelationalCoordinates(List<Focus> focuses, ProcessingOptions options)
        {
            foreach (var focus in focuses)
            {
                if (focus.Prerequisites.Any())
                {
                    var parentName = focus.Prerequisites.First();
                    SetRelativeCoordinates(focuses, options, focus, parentName);
                }
                else if (focus.NonDisablingPrerequisites.Any())
                {
                    var parentName = focus.NonDisablingPrerequisites.First();
                    SetRelativeCoordinates(focuses, options, focus, parentName);
                }
                else
                {
                    focus.RelativeX = (int)Math.Round(focus.X / options.CoordinatesDelimeterX);
                    focus.RelativeY = (int)Math.Round(focus.Y / options.CoordinatesDelimeterY);
                }
            }
        }

        private void SetRelativeCoordinates(List<Focus> focuses, ProcessingOptions options, Focus focus, string parentName)
        {
            var parent = focuses.First(x => x.Name.Equals(parentName));
            focus.ParentName = parentName;
            var xDiff = focus.X - parent.X;
            var yDiff = focus.Y - parent.Y;
            var relativeX = (int)Math.Round(xDiff / options.CoordinatesDelimeterX);
            var relativeY = (int)Math.Round(yDiff / options.CoordinatesDelimeterY);
            focus.RelativeX = relativeX;
            focus.RelativeY = relativeY;
        }

        private List<MxCell> GetItems(Mxfile mxfile)
        {
            var items = mxfile.Diagram?.MxGraphModel?.Root?.MxCell ?? throw new ArgumentException("File has incorrect formatting");
            return items;
        }

        private string ProcessFocusName(string? name, ProcessingOptions options)
        {
            if (string.IsNullOrWhiteSpace(name))
            {
                throw new ArgumentException("Name of focus is missing");
            }

            var trimmed = Regex.Replace(name, @"<[^>]+>|&nbsp;", "").Trim();

            return $"{options.Tag}_{trimmed.Replace(' ', '_')}";
        }

        private bool ProcessDash(string? properties)
        {
            if (string.IsNullOrWhiteSpace(properties))
            {
                throw new ArgumentException("Style of line is missing");
            }

            var dashed = properties.Split(Constants.Xml.PropertiesDelimeter).FirstOrDefault(x => x.StartsWith("dashed="));
            if (dashed is null)
            {
                return false;
            }

            var parts = dashed.Split(Constants.Xml.ValueDelimeter);
            if (parts.Length == 2)
            {
                var value = parts[1];
                if (int.TryParse(value, out int result))
                {
                    return result == 1;
                }
            }

            return false;
        }

        private bool ProcessLink(string? properties)
        {
            if (string.IsNullOrWhiteSpace(properties))
            {
                throw new ArgumentException("Style of line is missing");
            }

            var shape = properties.Split(Constants.Xml.PropertiesDelimeter).FirstOrDefault(x => x.StartsWith("shape="));
            if (shape is null)
            {
                return false;
            }

            var parts = shape.Split(Constants.Xml.ValueDelimeter);
            if (parts.Length == 2)
            {
                var value = parts[1];
                if (value.Equals("link"))
                {
                    return true;
                }
            }

            return false;
        }
    }
}
