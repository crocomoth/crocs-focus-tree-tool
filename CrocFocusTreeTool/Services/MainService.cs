﻿using CrocFocusTreeTool.Models;

namespace CrocFocusTreeTool.Services
{
    public class MainService
    {
        private readonly TreeProcessor _treeProcessor;
        private readonly XmlParser _xmlParser;
        private readonly DocumentProcessor _documentProcessor;
        private readonly FocusTreeRewardService _rewardService;
        private readonly LocalisationService _localizationService;
        private readonly string _rewardsPath = AppDomain.CurrentDomain.BaseDirectory + "rewards.txt";

        public MainService()
        {
            _treeProcessor = new TreeProcessor();
            _xmlParser = new XmlParser();
            _rewardService = new FocusTreeRewardService();
            _localizationService = new LocalisationService();
            _documentProcessor = new DocumentProcessor(_rewardService);
        }

        public void Process(string input, string rewards, string output, ProcessingOptions options)
        {
            if (!File.Exists(input))
            {
                throw new FileNotFoundException(input);
            }

            var data = File.ReadAllText(input);
            var mxFile = _xmlParser.ParseXml(data);

            //var rewardData = File.ReadAllLines(rewards);
            //_rewardService.SetUpTokens(rewardData);
            //var simpleRewards = File.ReadAllText(_rewardsPath);
            //_rewardService.SetUpSimpleRewards(simpleRewards);

            var focuses = _treeProcessor.Process(mxFile, options);
            var result = _documentProcessor.CreateDocument(focuses, options);
            var loc = _localizationService.CreateLocalisation(focuses);

            if (!output.EndsWith('\\'))
            {
                output += '\\';
            }

            var locFile = output + "loc.yml";
            var focusFile = output + "focus_tree.txt";

            File.WriteAllText(locFile, loc);
            File.WriteAllText(focusFile, result);
        }
    }
}
