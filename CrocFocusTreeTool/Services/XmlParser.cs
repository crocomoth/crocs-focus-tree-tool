﻿using CrocFocusTreeTool.Models;
using System.Xml.Serialization;

namespace CrocFocusTreeTool.Services
{
    public class XmlParser
    {
        public Mxfile ParseXml(string xml)
        {
            Mxfile? result;
            var serializer = new XmlSerializer(typeof(Mxfile));
            using (var reader = new StringReader(xml))
            {
                var obj = serializer.Deserialize(reader);
                if (obj is null)
                {
                    throw new Exception("Could not deserialize the file");
                }

                result = (Mxfile)obj;
            }

            if (result is null)
            {
                throw new Exception("Could not parse XML correctly");
            }

            return result;
        }
    }
}
