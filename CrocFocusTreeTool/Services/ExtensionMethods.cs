﻿namespace CrocFocusTreeTool.Services
{
    public static class ExtensionMethods
    {
        public static string Tab(this string value, int times)
        {
            return $"{new string('\t', times)}{value}";
        }
    }
}
