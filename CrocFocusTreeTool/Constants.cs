﻿namespace CrocFocusTreeTool
{
    public static class Constants
    {
        public static class Commands
        {
            public const string Help = "help";
            public const string Name = "name";
            public const string XDel = "x-del";
            public const string YDel = "y-del";
            public const string XCont = "x-cont";
            public const string YCont = "Y-cont";
            public const string Icon = "icon";
            public const string Ai = "ai";
            public const string Cost = "cost";
            public const string Filter = "filter";

            public const char CommandsDelimeter = ';';
            public const char ValueDelimeter = '=';
        }

        public static class Xml
        {
            public const char PropertiesDelimeter = ';';
            public const char ValueDelimeter = '=';
        }
    }
}
